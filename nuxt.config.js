import meta from './meta.json'

export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: meta.name || 'Mochi - Vaanres',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: meta.description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'prerender', href: '/' },
      { rel: 'prerender', href: '/gallery' }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/Loading.vue',
  // loading: { color: '#424242' },
  loadingIndicator: '~assets/loading.html',

  /* loadingIndicator: {
    name: 'folding-cube',
    color: '#424242',
    background: '#f3f2f1'
  }, */

  layoutTransition: {
    name: 'layout',
    mode: 'out-in'
  },
  /*
   ** Global CSS
   */
  css: ['~assets/styles/main'], // 'swiper/css/swiper.min.css'
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/vue-lazyload',
    '~/plugins/utils.js',
    {
      src: '~plugins/vue-scrollmagic.js',
      ssr: false
    }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'nuxt-webfontloader',
    [
      'bootstrap-vue/nuxt',
      {
        bootstrapCss: false
      }
    ],
    '@nuxtjs/style-resources',
    [
      '@nuxtjs/component-cache',
      {
        max: 10000,
        maxAge: 1000 * 60 * 60
      }
    ]
    // '@nuxtjs/axios'
    // '@nuxtjs/pwa'
  ],

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  // axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        name: true
      }
    }
  },
  webfontloader: {
    custom: {
      families: [
        'Playfair Display:n4,n7',
        'Montserrat:n4,n5,n6,n7',
        'Material Icons'
      ],
      urls: [
        'https://fonts.googleapis.com/icon?family=Montserrat:400,500,600,700|Playfair+Display:400,700&display=swap',
        'https://fonts.googleapis.com/icon?family=Material+Icons&display=block'
      ]
    }
  },
  styleResources: {
    scss: ['~assets/styles/_core.scss']
  }
}
