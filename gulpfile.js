const path = require('path')
const { series, parallel, src, dest } = require('gulp')
// const fs = require('fs-extra')
// const sharp = require('sharp')
// const glob = require('glob')
const del = require('del')
const imagemin = require('gulp-imagemin')
const webp = require('gulp-webp')
const mozjpeg = require('imagemin-mozjpeg')
const pngquant = require('imagemin-pngquant')
const svgo = require('imagemin-svgo')
const IMG_ASSETS = 'static/img/'

function getParentDir(filePath) {
  return path.basename(path.dirname(filePath))
}

function getCurrentDir(filePath) {
  return path.basename(filePath)
}

function getOptimizedPath(suffix = 'optimized') {
  const parentDir = getParentDir(IMG_ASSETS)
  const currentDir = getCurrentDir(IMG_ASSETS)
  const destination = `${parentDir}/${currentDir}_${suffix}/`
  return destination
}

function cleanDestination(done) {
  del(getOptimizedPath())
  done()
}

function convertToWebP() {
  const srcPath = `${IMG_ASSETS}**/*`
  const destPath = getOptimizedPath()

  return src(srcPath)
    .pipe(
      webp({
        quality: 80
      })
    )
    .pipe(dest(destPath))
}

function compressImages() {
  const srcPath = `${IMG_ASSETS}**/*`
  const destPath = getOptimizedPath()

  return src(srcPath)
    .pipe(
      imagemin([
        pngquant({ quality: [0.5, 0.8] }),
        mozjpeg({ quality: 80 }),
        svgo({ removeViewBox: true })
      ])
    )
    .pipe(dest(destPath))
}

const defaultTasks = series(
  cleanDestination,
  parallel(convertToWebP, compressImages)
)

exports.default = defaultTasks
