export const state = () => ({
  position: {
    x: 0,
    y: 0
  }
})

export const mutations = {
  update(state, position) {
    state.position.x = position.x
    state.position.y = position.y
  }
}
