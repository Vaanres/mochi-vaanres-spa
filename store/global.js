export const state = () => ({
  loadingStatus: false
})

export const mutations = {
  updateLoadingStatus(state, status) {
    state.loadingStatus = status
  }
}
